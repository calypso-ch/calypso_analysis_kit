import argparse


def reformat(input):
    print("Function reformat")


def add_subcommand(subparsers):
    subparser = subparsers.add_parser('reformat')
    subparser.add_argument('--input')
    subparser.set_defaults(func=reformat)
    return subparsers
