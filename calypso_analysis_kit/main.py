import argparse

from calypso_analysis_kit import analy, reformat


def default_msg(args):
    print("Unkown options, please run `cak.py -h` for more help.")


def parse_args():
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default_msg)
    subparsers = parser.add_subparsers(title='Choose functions')

    subparsers = analy.add_subcommand(subparsers)
    subparsers = reformat.add_subcommand(subparsers)

    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
