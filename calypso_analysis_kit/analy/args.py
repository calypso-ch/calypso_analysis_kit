import argparse

from .analy import analy


def add_subcommand(subparsers):
    subparser = subparsers.add_parser('analy')

    subparser.add_argument(
        '-i',
        '--input',
        default='caly.ini',
        help="input config file (default: caly.ini)",
    )
    subparser.add_argument(
        '-r',
        '--results',
        default='results',
        help="CALYPSO results dir (default: results)",
    )
    subparser.add_argument(
        '-n',
        type=int,
        default=50,
        help="number of sorted structures to output, set a negtive "
        "number to process all. (default: 50)"
    )
    subparser.add_argument(
        '-t',
        '--tolerances',
        nargs='+',
        type=float,
        default=[0.01],
        help="symmetry tolerances",
    )
    subparser.add_argument(
        '--fmt',
        nargs='*',
        default=['vasp'],
        help="write out format, support vasp|cif|xyz (default: vasp)",
    )
    subparser.add_argument(
        '--rm-sim',
        action='store_true',
        help="REMOVE duplicated similar structures by fingerprints."
    )
    subparser.add_argument(
        '--fp',
        '--fingerprints',
        default=None,
        help="Fingerprints of each optimized structure calculated by CALYPSO. "
        "Will calculate again if not exists. (default: ${results}/caly_fp.npy)"
    )

    subparser.set_defaults(func=analy)
    return subparsers
