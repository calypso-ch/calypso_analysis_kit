from functools import partial
from itertools import groupby
from pathlib import Path

import numpy as np
import pandas as pd
from ase import Atoms
from ase.io import read, write
from ase.spacegroup import Spacegroup, get_spacegroup
from spglib import refine_cell

try:
    from calypso.config.read_input import Config
    from calypso.fileio import load
    from calypso.fingerprints.fingerprint import FingerPrint
except ImportError:
    raise ImportError("CALYPSO not installed")


def analy(args):
    # print(vars(args))
    config = Config().from_file(args.input)

    keys = []
    sort_key = 'enthalpy_per_atom'
    ascending = True
    if config.get('critic') != 'enthalpy':
        keys.append(config.get('critic'))
        sort_key = config.get('critic')
    if sort_key == 'hardness':
        ascending = False

    # ini = load(Path(args.results).joinpath('caly_ini.caly'))
    opt = load(Path(args.results).joinpath('caly_opt.caly'))

    # TODO: filter similar structures by fingerprints
    # if args.fp is None:
    #     # fp = np.load(Path(args.results).joinpath('caly_fp.npy'))
    #     pass
    # else:
    #     # calculate by CALYPSO
    #     # fp = np.load(args.fp)
    #     raise FileNotFoundError(f"{args.fp} does not exist. Not supported yet")

    # {f1: [a1, a2], f2: [a3, a4]} where f1 and f2 are reduced formula
    reduced_formula_dict = {
        k: list(v)
        for k, v in groupby(
            opt, lambda atoms: atoms.symbols.formula.reduce()[0].format('metal')
        )
    }

    for reduced_formula, atoms_list in reduced_formula_dict.items():
        formula_dir = Path(args.results).joinpath(reduced_formula)
        formula_dir.mkdir(exist_ok=True)

        df = pd.DataFrame()  # columns: 'sid', keys, args.tolerances
        opt_d = {}  # {sid: atoms}
        for atoms in atoms_list:
            opt_d[atoms.info['sid']] = atoms
            info = {
                'sid': atoms.info['sid'],
                'formula': atoms.get_chemical_formula('metal'),
                'natoms': len(atoms),
                'enthalpy_per_atom': atoms.info['enthalpy'] / len(atoms),
            }
            info.update({key: atoms.info.get(key, np.nan) for key in keys})
            for tol in args.tolerances:
                no, sym = get_spgno(atoms, tol)
                info.update({f'spg-{tol}': no, f'symb-{tol}': sym})
            info = pd.Series(info)
            df = pd.concat([df, info], axis=1, ignore_index=True)
        df = df.T
        df = df.sort_values(by=sort_key, ascending=ascending)
        if args.n >= 0:
            df = df[: args.n]

        # write out Analysis.txt
        with open(formula_dir.joinpath('Analysis.txt'), 'w') as f:
            title = ' '.join(map(wrap_strfmt, df.columns))
            f.write(title + '\n')
            for _, row in df.iterrows():
                row = ' '.join(map(wrap_strfmt, row))
                f.write(row + '\n')

        # write out refined atoms to file
        print("Refining structures and writing out")
        for sid in df.sid:
            for tol in args.tolerances:
                dir_name = formula_dir.joinpath(f'dir_{tol}')
                dir_name.mkdir(exist_ok=True)
                refined_atoms = refine_atoms(opt_d[sid], tol)
                for fmt in args.fmt:
                    write(
                        dir_name.joinpath(f"{sid}_{sym}.{fmt}"),
                        refined_atoms,
                    )


def get_spgno(atoms, tolerance):
    try:
        sg = get_spacegroup(atoms)
    except RuntimeError:
        sg = Spacegroup(1)
    return sg.no, sg.symbol.replace(' ', '')


def refine_atoms(atoms, tolerance):
    pbc = atoms.pbc
    lat = atoms.get_cell()
    pos = atoms.get_scaled_positions()
    num = atoms.get_atomic_numbers()
    # lat, pos, num
    _cell = refine_cell((lat, pos, num), tolerance)
    if _cell is not None:
        return Atoms(_cell[2], cell=_cell[0], scaled_positions=_cell[1], pbc=pbc)
    else:
        return atoms


def strfmt(obj, length=18):
    if isinstance(obj, float):
        fmt = f"{{:>{length}.9f}}"
    else:
        fmt = f"{{:>{length}}}"
    return fmt.format(obj)


wrap_strfmt = partial(strfmt, length=18)
